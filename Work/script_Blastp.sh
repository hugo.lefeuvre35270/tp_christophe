#!/bin/bash
#stocker le chemin du dossier qui contient les bases de données dans une variable
BDdir=/home/ctatout/WORK/BDdir  

#Aller chercher les fichiers DB
SAMP=$(find $BDdir -name "*.phr" -type f) 

#stocker chaque nom de fichier BDD dans la variable $i en faisant une boucle
for i in ${SAMP[@]} ; 
do 
database="${i%.*}"        

#Créer une variable qui contiendra le nom de fichier de résultat pour chaque BDD
F1=$(basename $BDdir/${i%.*}).txt          

#Lancer la commande blast sur les base de données de chaque espèce
blastp -db $database  -query SEQ_KNN -out $F1 -outfmt 6 -evalue 1e-05 -max_target_seqs 1
done